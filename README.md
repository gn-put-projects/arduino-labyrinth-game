# Arduino Labyrinth Game

A simple labyrinth game designed to run on the Arduino Uno with a 16x16 matrix RGB LED display.

Use a joystick to navigate to the labyrinth's exit in three difficulty levels! Every labyrinth is generated in real time as you start the game so get ready for a unique experience each time. Good luck and have fun!