#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define DISPLAY_PIN           6
#define UNCONNECTED_PIN      12

#define JOYSTICK_X           A0
#define JOYSTICK_Y           A1
#define JOYSTICK_BUTTON       2

#define JOYSTICK_IDLE_MIN   300
#define JOYSTICK_IDLE_MAX   700

#define DISPLAY_WIDTH        16
#define DISPLAY_HEIGHT       16
#define PIXEL_COUNT         256

Adafruit_NeoPixel NeoPixel(PIXEL_COUNT, DISPLAY_PIN, NEO_GRB + NEO_KHZ800);

#define VOID 0
#define WALL 1
#define PATH 2

#define RIGHT 0
#define LEFT 1
#define DOWN 2
#define UP 3

// State of the joystick button. 0 - released ; 1 - pressed
int button = 0;

#define PLAYING 1
#define FINISHED 2
#define DISPLAY_SCORE 3
#define SCORE 4

#define DISPLAY_EASY 5
#define MENU_EASY 6

#define DISPLAY_MEDIUM 7
#define MENU_MEDIUM 8

#define DISPLAY_HARD 9
#define MENU_HARD 10

/*
// Variable holding the game's current state
*/
int gameState;

byte letters[28][5] = {
  {
    0B01000000,
    0B10100000,
    0B11100000,
    0B10100000,
    0B10100000
  },
  {
    0B11000000,
    0B10100000,
    0B11000000,
    0B10100000,
    0B11000000
  },
  {
    0B11100000,
    0B10000000,
    0B10000000,
    0B10000000,
    0B11100000
  },
  {
    0B11000000,
    0B10100000,
    0B10100000,
    0B10100000,
    0B11000000
  },
  {
    0B11100000,
    0B10000000,
    0B11000000,
    0B10000000,
    0B11100000
  },
  {
    0B11100000,
    0B10000000,
    0B11000000,
    0B10000000,
    0B10000000
  },
  {
    0B11100000,
    0B10000000,
    0B10100000,
    0B10100000,
    0B11100000
  },
  {
    0B10100000,
    0B10100000,
    0B11100000,
    0B10100000,
    0B10100000
  },
  {
    0B10000000,
    0B10000000,
    0B10000000,
    0B10000000,
    0B10000000
  },
  {
    0B00100000,
    0B00100000,
    0B00100000,
    0B10100000,
    0B11100000
  },
  {
    0B10100000,
    0B10100000,
    0B11000000,
    0B11000000,
    0B10100000
  },
  {
    0B10000000,
    0B10000000,
    0B10000000,
    0B10000000,
    0B11100000
  },
  {
    0B10001000,
    0B11011000,
    0B10101000,
    0B10001000,
    0B10001000
  },
  {
    0B10010000,
    0B11010000,
    0B10110000,
    0B10010000,
    0B10010000
  },
  {
    0B11100000,
    0B10100000,
    0B10100000,
    0B10100000,
    0B11100000
  },
  {
    0B11100000,
    0B10100000,
    0B11100000,
    0B10000000,
    0B10000000
  },
  {
    0B11100000,
    0B10100000,
    0B10100000,
    0B10100000,
    0B11110000
  },
  {
    0B11100000,
    0B10100000,
    0B11100000,
    0B11000000,
    0B10100000
  },
  {
    0B11100000,
    0B10000000,
    0B11100000,
    0B00100000,
    0B11100000
  },
  {
    0B11100000,
    0B01000000,
    0B01000000,
    0B01000000,
    0B01000000
  },
  {
    0B10100000,
    0B10100000,
    0B10100000,
    0B10100000,
    0B11100000
  },
  {
    0B10100000,
    0B10100000,
    0B10100000,
    0B10100000,
    0B01000000
  },
  {
    0B10001000,
    0B10001000,
    0B10001000,
    0B10101000,
    0B01010000
  },
  {
    0B10100000,
    0B10100000,
    0B01000000,
    0B10100000,
    0B10100000
  },
  {
    0B10100000,
    0B10100000,
    0B01000000,
    0B01000000,
    0B01000000
  },
  {
    0B11100000,
    0B00100000,
    0B01000000,
    0B10000000,
    0B11100000
  },
  {
    0B00100000,
    0B01000000,
    0B10000000,
    0B01000000,
    0B00100000
  },
  {
    0B10000000,
    0B01000000,
    0B00100000,
    0B01000000,
    0B10000000
  }
};

byte digits[12][5] = {
  {
    0B01000000,
    0B10100000,
    0B10100000,
    0B10100000,
    0B01000000
  },
  {
    0B01000000,
    0B11000000,
    0B01000000,
    0B01000000,
    0B01000000
  },
  {
    0B11100000,
    0B00100000,
    0B11100000,
    0B10000000,
    0B11100000
  },
  {
    0B11100000,
    0B00100000,
    0B11100000,
    0B00100000,
    0B11100000
  },
  {
    0B10100000,
    0B10100000,
    0B11100000,
    0B00100000,
    0B00100000
  },
  {
    0B11100000,
    0B10000000,
    0B11100000,
    0B00100000,
    0B11100000
  },
  {
    0B11100000,
    0B10000000,
    0B11100000,
    0B10100000,
    0B11100000
  },
  {
    0B11100000,
    0B00100000,
    0B00100000,
    0B00100000,
    0B00100000
  },
  {
    0B11100000,
    0B10100000,
    0B11100000,
    0B10100000,
    0B11100000
  },
  {
    0B11100000,
    0B10100000,
    0B11100000,
    0B00100000,
    0B11100000
  },
  {
    0B01100000,
    0B10000000,
    0B01000000,
    0B00100000,
    0B11000000
  },
  {
    0B00000000,
    0B00000000,
    0B00000000,
    0B00000000,
    0B10000000
  }
};

#define WHITE NeoPixel.Color(255, 255, 255)
#define BLACK NeoPixel.Color(0, 0, 0)
#define GREEN NeoPixel.Color(0, 255, 0)
#define YELLOW NeoPixel.Color(200, 200, 0)
#define RED NeoPixel.Color(255, 0, 0)
#define BLUE NeoPixel.Color(0, 0, 255)

#define MODE_EASY 0
#define MODE_MEDIUM 1
#define MODE_HARD 2

#define LABYRINTH_WIDTH_EASY 15
#define LABYRINTH_HEIGHT_EASY 15
#define PLAYER_COLOR_EASY NeoPixel.Color(255, 0, 0)
#define WALL_COLOR_EASY NeoPixel.Color(0, 0, 255)
#define PATH_COLOR_EASY NeoPixel.Color(255, 255, 0)
#define EXIT_COLOR_EASY NeoPixel.Color(0, 255, 0)

#define LABYRINTH_WIDTH_MEDIUM 21
#define LABYRINTH_HEIGHT_MEDIUM 21
#define PLAYER_COLOR_MEDIUM NeoPixel.Color(255, 255, 0)
#define WALL_COLOR_MEDIUM NeoPixel.Color(255, 255, 255)
#define PATH_COLOR_MEDIUM NeoPixel.Color(0, 0, 0)
#define EXIT_COLOR_MEDIUM NeoPixel.Color(0, 255, 255)

#define LABYRINTH_WIDTH_HARD 25
#define LABYRINTH_HEIGHT_HARD 25
#define PLAYER_COLOR_HARD NeoPixel.Color(255, 255, 255)
#define WALL_COLOR_HARD NeoPixel.Color(255, 0, 0)
#define PATH_COLOR_HARD NeoPixel.Color(100, 100, 255)
#define EXIT_COLOR_HARD NeoPixel.Color(0, 255, 0)

// Maximum labyrinth's dimentions in tiles
#define MAX_LABYRINTH_WIDTH 27
#define MAX_LABYRINTH_HEIGHT 27

#define BITS_IN_BYTE 8
#define TILE_BITS_COUNT 2

// How many tiles are encoded in one byte?
#define TILES_IN_ONE_BYTE (BITS_IN_BYTE/TILE_BITS_COUNT)

// On how many bytes is one row encoded?
#define BYTES_IN_ROW ((MAX_LABYRINTH_WIDTH+TILES_IN_ONE_BYTE-1)/TILES_IN_ONE_BYTE)

// Camera's coordinates and size
int cameraX = 0;
int cameraY = 0;
int cameraWidth = DISPLAY_WIDTH;
int cameraHeight = DISPLAY_HEIGHT;

/*
// Read joystick's position X and return 1 on right move,
// -1 on left move and 0 on lack of movement
*/
int readJoystickX() {
  int joy_x = analogRead(JOYSTICK_X);
  if (joy_x > JOYSTICK_IDLE_MAX) return 1; 
  else if (joy_x < JOYSTICK_IDLE_MIN) return -1;
  return 0;
}

/*
// Read joystick's position Y and return 1 on upward move,
// -1 on downward move and 0 on lack of movement
*/
int readJoystickY() {
  int joy_y = analogRead(JOYSTICK_Y);
  if (joy_y > JOYSTICK_IDLE_MAX) return 1; 
  else if (joy_y < JOYSTICK_IDLE_MIN) return -1;
  return 0;
}

/*
// Update the variable button to prepresent the current
// state of the joystick's button (0 - released, 1 - pressed).
*/
int readJoystickButton() {
  button = !digitalRead(JOYSTICK_BUTTON);
}

/*
// Set a pixel at given coordinated to the given color.
*/
void setPixel(int px, int py, uint32_t color) {
  if (px < cameraX || px >= cameraX+cameraWidth) return;
  if (py < cameraY || py >= cameraY+cameraHeight) return;

  int x = px - cameraX;
  int y = py - cameraY;

  int pixel = 0;
  if(y % 2 == 0) pixel = DISPLAY_WIDTH-1-x+(DISPLAY_WIDTH-1-y)*DISPLAY_HEIGHT;
  else pixel = (DISPLAY_WIDTH-1-y)*DISPLAY_HEIGHT + x;
  NeoPixel.setPixelColor(pixel, color);
}

/*
// Sets a group of pixels in a specified rectangle to a given color.
*/
void fillRect(int sx, int sy, int width, int height, uint32_t color) {
  for (int x=sx; x < sx+width; ++x) {
    for (int y=sy; y < sy+height; ++y) {
      setPixel(x, y, color);
    }
  }
}

// Displays given char on the LED display. Returns the width
// of the displayed character or -1 if the character was not recognized
int displayChar(int dx, int dy, char c, uint32_t color=WHITE) {
  if (c >= '0' && c <= '9' || c == 's' || c == '.') {
    if (c == 's') c = '9'+1;
    if (c == '.') c = '9'+2;

    c -= '0';
    
    int maxWidth = 0;
    for (int y=0; y < 5; ++y) {
      if (digits[c][y] & 0B10000000) {
        maxWidth = max(maxWidth, 1);
        setPixel(dx, dy+y, color);
      }
      if (digits[c][y] & 0B01000000) {
        maxWidth = max(maxWidth, 2);
        setPixel(dx+1, dy+y, color);
      }
      if (digits[c][y] & 0B00100000) {
        maxWidth = max(maxWidth, 3);
        setPixel(dx+2, dy+y, color);
      }
      if (digits[c][y] & 0B00010000) {
        maxWidth = max(maxWidth, 4);
        setPixel(dx+3, dy+y, color);
      }
      if (digits[c][y] & 0B00001000) {
        maxWidth = max(maxWidth, 5);
        setPixel(dx+4, dy+y, color);
      }
    }

    return maxWidth;
  }
  if (c >= 'A' && c <= 'Z' || c == '<' || c == '>') {
    if (c == '<') c = 'Z'+1;
    if (c == '>') c = 'Z'+2;

    c -= 'A';
    int maxWidth = 0;
    for (int y=0; y < 5; ++y) {
      if (letters[c][y] & 0B10000000) {
        maxWidth = max(maxWidth, 1);
        setPixel(dx, dy+y, color);
      }
      if (letters[c][y] & 0B01000000) {
        maxWidth = max(maxWidth, 2);
        setPixel(dx+1, dy+y, color);
      }
      if (letters[c][y] & 0B00100000) {
        maxWidth = max(maxWidth, 3);
        setPixel(dx+2, dy+y, color);
      }
      if (letters[c][y] & 0B00010000) {
        maxWidth = max(maxWidth, 4);
        setPixel(dx+3, dy+y, color);
      }
      if (letters[c][y] & 0B00001000) {
        maxWidth = max(maxWidth, 5);
        setPixel(dx+4, dy+y, color);
      }
    }

    return maxWidth;
  }
  return -1;
}

/*
// Displays a string at given coordinates, repeatedly calling `displayChar`.
*/
void displayString(int dx, int dy, String str, uint32_t color=WHITE) {
  int width = 0;
  for (int i=0; i < str.length(); ++i){
    width += displayChar(dx+width, dy, str.charAt(i), color);
    width++;
  }
}

/*
// Class responsible for generating and storing labyrinths.
*/
class Labyrinth {
  public:
  // An array holding information on the current labyrinth's layout
  byte arr[(MAX_LABYRINTH_WIDTH*MAX_LABYRINTH_HEIGHT)/TILES_IN_ONE_BYTE];
  // The labyrinth's dimentions
  int width;
  int height;
  // Position of the labyrinth's exit
  int exitX;
  int exitY;

  // Colors of the current labyrinth
  uint32_t pathColor;
  uint32_t wallColor;
  uint32_t exitColor;
  
  /*
  // Returns the value describing the content of a tile at given coordinates.
  */
  int at(int x, int y) {
    if (x >= MAX_LABYRINTH_WIDTH || x < 0) return -1;
    if (y >= MAX_LABYRINTH_HEIGHT || y < 0) return -1;
    
    int byteIndex = y*BYTES_IN_ROW + x/TILES_IN_ONE_BYTE;

    String log = String(x) + " / " + String(TILES_IN_ONE_BYTE) + String(" = ") + String(x/TILES_IN_ONE_BYTE);

    switch(x % TILES_IN_ONE_BYTE) {
      case 0:
        return (arr[byteIndex] & 0B11000000) >> 6;
      case 1:
        return (arr[byteIndex] & 0B00110000) >> 4;
      case 2:
        return (arr[byteIndex] & 0B00001100) >> 2;
      case 3:
        return (arr[byteIndex] & 0B00000011);
    }
  }

  /* Sets a tile at given coordinates (x,y) to the provided value with bound checking.
  // Tile information is stored in 2 bits. An example byte from this->arr follows:
  // 0B 00 01 10 00
  // This byte stores information about 4 tiles, which values are, in decimal: 0, 1, 2 and 0.
  // This means there is VOID on the first tile, a WALL on the second,
  // a PATH on the third and VOID on the fourth
  */
  void set(int x, int y, int value) {
    // Bound checking
    if (x >= this->width || x < 0) return;
    if (y >= this->height || y < 0) return;
    
    // Calculate the index of the byte which contains the changed tile's information
    int byteIndex = y*BYTES_IN_ROW + x/TILES_IN_ONE_BYTE;

    // One byte has information on four tiles. This switch ensures the correct
    // two bits are altered and the rest is left intact.
    switch(x % TILES_IN_ONE_BYTE) {
      case 0:
        arr[byteIndex] = (value << 6) | (arr[byteIndex] & 0B00111111);
        break;
      case 1:
        arr[byteIndex] = (value << 4) | (arr[byteIndex] & 0B11001111);
        break;
      case 2:
        arr[byteIndex] = (value << 2) | (arr[byteIndex] & 0B11110011);
        break;
      case 3:
        arr[byteIndex] = (value << 0) | (arr[byteIndex] & 0B11111100);
        break;
    }
  }

  bool outOfBounds(int x, int y) {
    return x<0 || x >= this->width || y<0 || y >= this->height;
  }

  /*
  // Checks if the tile at given coordinates is VOID.
  */
  bool isVoid(int x, int y) {
    if (outOfBounds(x, y)) return false;
    return this->at(x, y) == VOID;
  }

  /*
  // Checks if the tile at given coordinates is WALL.
  */
  bool isWall(int x, int y) {
    if (outOfBounds(x, y)) return false;
    return this->at(x, y) == WALL;
  }

  /*
  // Checks if the tile at given coordinates is PATH.
  */
  bool isPath(int x, int y) {
    if (outOfBounds(x, y)) return false;
    return this->at(x, y) == PATH;
  }

  /*
  // Resets the labyrinth making it ready for generation.
  */
  void clear() {
    for (int x=0; x < this->width; ++x) {
      for (int y=0; y < this->height; ++y) {
        if (((x+1)*(y+1)) % 2 == 1) {
          this->set(x, y, VOID);
          this->redraw(x, y, true);
        }
        else {
          this->set(x, y, WALL);
          this->redraw(x, y, true);
        }
      }
    }
  }

  /*
  // Draw the whole labyrinth
  */
  void draw() {
    for (int x=0; x < this->width; x++) {
      for (int y=0; y < this->height; y++) {
        redraw(x, y);
      }
    }
  }

  /*
  // Redraw the specified pixel. Optionally refresh the display afterwards
  */
  void redraw(int x, int y, bool refresh=false) {
    if (x == exitX && y == exitY) {
      setPixel(x, y, exitColor);
    }
    else if (this->at(x, y) == WALL){
      setPixel(x, y, wallColor);
    }
    else if (this->at(x, y) == PATH){
      setPixel(x, y, pathColor);
    }
    else {
      // tile is VOID
      setPixel(x, y, BLACK);
    }
    if (refresh) NeoPixel.show();
  }
  
  /*
  // Turns off the LEDs in a diagonal pattern.
  */
  void animate() {
    int maxX = this->width-1;
    int maxY = this->height-1;
    // Time after which the screen should be refreshed. Less than 0 means always
    // refresh after changing a pixel
    int refreshTime = -1;

    int lastRefreshTime = millis();
    for (int s=maxX; s>=0; --s){
      for (int x=0,y=0; s+x < this->width; ++x,++y) {
        this->set(s+x, maxY-y, VOID);
        if (int(millis())-lastRefreshTime > refreshTime) {
          this->redraw(s+x, maxY-y, true);
          lastRefreshTime = millis();
        }
        else this->redraw(s+x, maxY-y);
      }
    }
    for (int s=maxY; s>=0; --s){
      for (int x=0,y=0; s-y >= 0; ++x,++y) {
        this->set(x, s-y, VOID);
        if (int(millis())-lastRefreshTime > refreshTime) {
          this->redraw(x, s-y, true);
          lastRefreshTime = millis();
        }
        else this->redraw(x, s-y);
      }
    }
  }

  /*
  // Unsets the labyrinth's exit
  */
  void end() {
    this->exitX = -1;
    this->exitY = -1;
  }

  /*
  // Initiates labyrinth generation at the given difficulty mode.
  */
  void generate(int mode) {
    switch(mode) {
      case MODE_EASY:
        this->width = LABYRINTH_WIDTH_EASY;
        this->height = LABYRINTH_HEIGHT_EASY;
        this->wallColor = WALL_COLOR_EASY;
        this->pathColor = PATH_COLOR_EASY;
        this->exitColor = EXIT_COLOR_EASY;
        break;
      case MODE_MEDIUM:
        this->width = LABYRINTH_WIDTH_MEDIUM;
        this->height = LABYRINTH_HEIGHT_MEDIUM;
        this->wallColor = WALL_COLOR_MEDIUM;
        this->pathColor = PATH_COLOR_MEDIUM;
        this->exitColor = EXIT_COLOR_MEDIUM;
        break;
        case MODE_HARD:
        this->width = LABYRINTH_WIDTH_HARD;
        this->height = LABYRINTH_HEIGHT_HARD;
        this->wallColor = WALL_COLOR_HARD;
        this->pathColor = PATH_COLOR_HARD;
        this->exitColor = EXIT_COLOR_HARD;
        break;
    }
    this->exitX = this->width-1;
    this->exitY = this->height-1;
    this->clear();
    this->huntMode();
  }

  /*
  // Implementation of the "kill" part of the "Hunt and kill"
  // labyrinth generation algorithm
  */
  void killMode(int x, int y){
    while (true) {
      this->set(x, y, PATH);
      this->redraw(x, y, true);
      int options_count = 0;
      int available_options[4];

      if (isWall(x+1, y) && isVoid(x+2, y)) {
        available_options[options_count] = RIGHT;
        options_count++;
      }
      if (isWall(x-1, y) && isVoid(x-2, y)) {
        available_options[options_count] = LEFT;
        options_count++;
      }
      if (isWall(x, y+1) && isVoid(x, y+2)) {
        available_options[options_count] = DOWN;
        options_count++;
      }
      if (isWall(x, y-1) && isVoid(x, y-2)) {
        available_options[options_count] = UP;
        options_count++;
      }
      if (options_count == 0) break;

      int rand = random(0, options_count);
      int choice = available_options[rand];

      switch(choice) {
        case RIGHT:
          this->set(x+1, y, PATH);
          this->redraw(x+1, y, true);
          x += 2;
          break;
        case LEFT:
          this->set(x-1, y, PATH);
          this->redraw(x-1, y, true);
          x -= 2;
          break;
        case DOWN:
          this->set(x, y+1, PATH);
          this->redraw(x, y+1, true);
          y += 2;
          break;
        case UP:
          this->set(x, y-1, PATH);
          this->redraw(x, y-1, true);
          y -= 2;
          break;
      }
    }
  }

  /*
  // Implementation of the "hunt"" part of the "Hunt and kill"
  // labyrinth generation algorithm
  */
  void huntMode(){
    int x = 0;
    int y = 0;

    this->set(x, y, PATH);
    this->redraw(x, y, true);
    while(x < this->width && y < this->height){
      int options_count = 0;
      int available_options[2];

      if (isWall(x+1, y) && isVoid(x+2, y)) {
        available_options[options_count] = RIGHT;
        options_count++;
      }
      if (isWall(x, y+1) && isVoid(x, y+2)) {
        available_options[options_count] = DOWN;
        options_count++;
      }
        
      if (options_count == 0){
        x += 2;
        if(x >= this->width){
          x = 0;
          y += 2;
        }
      continue;
      }
        
      int rand = random(0, options_count);
      int choice = available_options[rand];
      
      switch(choice) {
        case RIGHT:
          this->set(x+1, y, PATH);
          this->redraw(x+1, y, true);
          killMode(x+2, y);
          break;
        case DOWN:
          this->set(x, y+1, PATH);
          this->redraw(x, y+1, true);
          killMode(x, y+2);
          break;
      }
    }
  }
};

/*
// Class responsible for moving and displaying the player
*/
class Player{
  public:
  // Player's position x
  int x;
  // Player's position y
  int y;
  // Player's color
  uint32_t color;

  /*
  // Draw player and refresh the screen
  */
  void draw() {
    setPixel(x, y, this->color);
    NeoPixel.show();
  }

  /*
  // Initialize the player before a level
  */
  void init(int mode) {
    x = 0;
    y = 0;
    switch(mode) {
      case MODE_EASY:
        this->color = PLAYER_COLOR_EASY;
        break;
      case MODE_MEDIUM:
        this->color = PLAYER_COLOR_MEDIUM;
        break;
      case MODE_HARD:
        this->color = PLAYER_COLOR_HARD;
        break;
    }
  }

  /*
  // Unsets the player's color
  */
  void end() {
    this->color = BLACK;
  }

  /*
  // Manage to joystick input
  */
  void update(Labyrinth labyrinth){
    int horizontal = readJoystickX();
    int vertical = readJoystickY();

    if(abs(horizontal) + abs(vertical) == 2)
      return;

    if (!labyrinth.isPath(x+horizontal, y+vertical))
      return;
    
    x += horizontal;
    y += vertical;

    if (x == labyrinth.exitX && y == labyrinth.exitY) {
      gameState = FINISHED;
    }
  }
};

/*
// Class responsible for the game
*/
class Game {
  public:
  // The labyrinth object used after game difficulty is selected
  Labyrinth labyrinth;
  // The player object
  Player player;
  // Variable used to measure level walkthrough time
  long gameTime;

  /*
  // Function run at the beginning of the program.
  // Used to welcome the player with the difficulty selection screen.
  */
  void init() {
    gameState = DISPLAY_EASY;
  }

  /*
  // Start the game at a specified difficulty level
  */
  void start(int mode) {
    this->player.init(mode);
    this->labyrinth.generate(mode);
    gameTime = long(millis());
    gameState = PLAYING;
  }

  /*
  // Update the game according to the current state
  */
  void update() {
    switch(gameState) {
      case PLAYING:
        this->player.update(this->labyrinth);
        cameraX = max(0, min(player.x - cameraWidth/2, this->labyrinth.width-cameraWidth));
        cameraY = max(0, min(player.y - cameraHeight/2, this->labyrinth.height-cameraHeight));
        this->draw();
        break;
      case FINISHED:
        gameTime = long(millis()) - gameTime;
        this->player.end();
        this->labyrinth.end();
        this->labyrinth.animate();
        cameraX = 0;
        cameraY = 0;
        gameState = DISPLAY_SCORE;
        break;
      case DISPLAY_SCORE:
        //Serial.print("finish time [ms]: ");
        //Serial.println(gameTime);
        displayString(1, 1, "NICE", GREEN);
        if (gameTime < 10000) {
          // display time in "4.5s" format
          displayString(1, 7, String(gameTime/1000) + String(".") + String((gameTime/100)%10) + String("s"), YELLOW);
        }
        else {
          // display time in "45s" format
          displayString(1, 7, String(gameTime/1000) + String("s"), YELLOW);
        }
        NeoPixel.show();
        gameState = SCORE;
        break;
      case SCORE:
        readJoystickButton();
        if (button) {
          this->init();
        }
        break;
      case DISPLAY_EASY:
        NeoPixel.clear();
        displayString(0, 1, "EASY", GREEN);
        displayString(7, 7, ">");
        NeoPixel.show();
        delay(350);
        gameState = MENU_EASY;
        break;
      case MENU_EASY:
        readJoystickButton();
        if (button) {
          this->start(MODE_EASY);
          return;
        }
        if (readJoystickX() == 1) {
          gameState = DISPLAY_MEDIUM;
        }
        break;
      case DISPLAY_MEDIUM:
        NeoPixel.clear();
        displayString(0, 1, "MEDI", YELLOW);
        displayString(1, 7, "<");
        displayString(12, 7, ">");
        NeoPixel.show();
        delay(350);
        gameState = MENU_MEDIUM;
        break;
      case DISPLAY_HARD:
        NeoPixel.clear();
        displayString(0, 1, "HARD", RED);
        displayString(6, 7, "<");
        NeoPixel.show();
        delay(350);
        gameState = MENU_HARD;
        break;
      case MENU_HARD:
        readJoystickButton();
        if (button) {
          this->start(MODE_HARD);
          return;
        }
        if (readJoystickX() == -1)
          gameState = DISPLAY_MEDIUM;
        break;
      case MENU_MEDIUM:
        readJoystickButton();
        if (button) {
          this->start(MODE_MEDIUM);
          return;
        }
        int joy = readJoystickX();
        if (joy == -1)
          gameState = DISPLAY_EASY;
        else if (joy == 1) {
          gameState = DISPLAY_HARD;
        }
        break;
    }
  }

  /*
  // Draw function to be called in game->update
  */
  void draw(){
    NeoPixel.clear();
    this->labyrinth.draw();
    this->player.draw();
  }
};

Game game;

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif

  pinMode(JOYSTICK_X, INPUT);
  pinMode(JOYSTICK_Y, INPUT);
  pinMode(JOYSTICK_BUTTON, INPUT_PULLUP);

  randomSeed(analogRead(UNCONNECTED_PIN));

  //Serial.begin(9600);

  NeoPixel.begin();
  NeoPixel.setBrightness(1);
  NeoPixel.clear();
  NeoPixel.show();

  game.init();
}

void loop() {
  game.update();
}